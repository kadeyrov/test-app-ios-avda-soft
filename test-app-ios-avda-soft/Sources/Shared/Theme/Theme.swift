import UIKit

enum Theme { }

extension Theme {
    enum Colors {
        static let blue: UIColor = .init(hexString: "#2C2B9C")
        static let dark: UIColor = .init(hexString: "#1B1D1F")
        static let red: UIColor = .init(hexString: "#DD7E7E")
        static let green: UIColor = .init(hexString: "#73C594")
        static let white: UIColor = .init(hexString: "#FFFFFF")
        static let text: UIColor = .init(hexString: "#9C9C9C")
        static let lightText: UIColor = .init(hexString: "#E0E0E0")
        static let yellow: UIColor = .init(hexString: "#FFD464")
        static let gray: UIColor = .init(hexString: "#828282")
        static let darkGray: UIColor = .init(hexString: "#4F4F4F")
        static let titlesDarkGray: UIColor = .init(hexString: "#333333")
        
        static let mainBackgroundColor: UIColor = white
        static let accentColor: UIColor = blue
        static let disabledColor: UIColor = UIColor.black.withAlphaComponent(0.2)
        static let mainColor: UIColor = blue
        
        static let statusBarStyleOnMain: UIStatusBarStyle = .lightContent
        static let mainActionButtonBackgroundColor: UIColor = blue
        static let textFieldTextColor: UIColor = gray
        static let navigationBarTextColor: UIColor = white
        
        static let actionButtonTitleColor: UIColor = white
        static let actionButtonEnabledColor: UIColor = accentColor
        static let actionButtonDisabledColor: UIColor = disabledColor

        static let lightButtonTitleColor: UIColor = accentColor
        static let lightButtonBackgroundColor: UIColor = white
        
        static let switchOnTintColor: UIColor = blue
        static let switchOffTintColor: UIColor = text
    }
}

private extension UIFont.Weight {
    var mulishFontName: String? {
        switch self {
        case .regular:
            return "Mulish-Regular"
        case .semibold:
            return "Mulish-SemiBold"
        case .bold:
            return "Mulish-Bold"
        default:
            return nil
        }
    }
}

extension Theme {
    enum Fonts {
        
        private static func font(for weight: UIFont.Weight, size: CGFloat = 17.0) -> UIFont {
            guard let name = weight.mulishFontName,
                let font = UIFont(name: name, size: size)
                else {
                    return UIFont.systemFont(ofSize: size, weight: weight)
            }
            return font
        }
        
        static let regularFont: UIFont = font(for: .regular)
        static let semibold: UIFont = font(for: .semibold)
        static let bold: UIFont = font(for: .bold)
    }
}
