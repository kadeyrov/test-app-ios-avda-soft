import UIKit

public protocol AlbumsSceneDisplayLogic: class {
    
    typealias Event = AlbumsScene.Event
    
    func display<#Event#>(viewModel: Event.<#Event#>.ViewModel)
}

extension AlbumsScene {
    
    public typealias DisplayLogic = AlbumsSceneDisplayLogic
    
    @objc(AlbumsSceneViewController)
    public class ViewController: UIViewController {
        
        public typealias Event = AlbumsScene.Event
        public typealias Model = AlbumsScene.Model
        
        // MARK: -
        
        deinit {
            self.onDeinit?(self)
        }
        
        // MARK: - Injections
        
        private var interactorDispatch: InteractorDispatch?
        private var routing: Routing?
        private var onDeinit: DeinitCompletion = nil
        
        public func inject(
            interactorDispatch: InteractorDispatch?,
            routing: Routing?,
            onDeinit: DeinitCompletion = nil
            ) {
            
            self.interactorDispatch = interactorDispatch
            self.routing = routing
            self.onDeinit = onDeinit
        }
        
        // MARK: - Overridden
        
        public override func viewDidLoad() {
            super.viewDidLoad()
            
//            let request = Event.<#Event#>.Request()
//            self.interactorDispatch?.sendRequest { businessLogic in
//                businessLogic.on<#Event#>(request: request)
//            }
        }
    }
}

extension AlbumsScene.ViewController: AlbumsScene.DisplayLogic {
    
    public func display<#Event#>(viewModel: Event.<#Event#>.ViewModel) {
        
    }
}
