import Foundation

public protocol AlbumsSceneBusinessLogic {
    
    typealias Event = AlbumsScene.Event
    
    func on<#Event#>(request: Event.<#Event#>.Request)
}

extension AlbumsScene {
    
    public typealias BusinessLogic = AlbumsSceneBusinessLogic
    
    @objc(AlbumsSceneInteractor)
    public class Interactor: NSObject {
        
        public typealias Event = AlbumsScene.Event
        public typealias Model = AlbumsScene.Model
        
        // MARK: - Private properties
        
        private let presenter: PresentationLogic
        
        // MARK: -
        
        public init(presenter: PresentationLogic) {
            self.presenter = presenter
        }
    }
}

extension AlbumsScene.Interactor: AlbumsScene.BusinessLogic {
    
    public func on<#Event#>(request: Event.<#Event#>.Request) {
        let response = Event.<#Event#>.Response()
        self.presenter.present<#Event#>(response: response)
    }
}
