import Foundation

public protocol AlbumsScenePresentationLogic {
    
    typealias Event = AlbumsScene.Event
    
    func present<#Event#>(response: Event.<#Event#>.Response)
}

extension AlbumsScene {
    
    public typealias PresentationLogic = AlbumsScenePresentationLogic
    
    @objc(AlbumsScenePresenter)
    public class Presenter: NSObject {
        
        public typealias Event = AlbumsScene.Event
        public typealias Model = AlbumsScene.Model
        
        // MARK: - Private properties
        
        private let presenterDispatch: PresenterDispatch
        
        // MARK: -
        
        public init(presenterDispatch: PresenterDispatch) {
            self.presenterDispatch = presenterDispatch
        }
    }
}

extension AlbumsScene.Presenter: AlbumsScene.PresentationLogic {
    
    public func present<#Event#>(response: Event.<#Event#>.Response) {
        let viewModel = Event.<#Event#>.ViewModel()
        self.presenterDispatch.display { displayLogic in
            displayLogic.display<#Event#>(viewModel: viewModel)
        }
    }
}
