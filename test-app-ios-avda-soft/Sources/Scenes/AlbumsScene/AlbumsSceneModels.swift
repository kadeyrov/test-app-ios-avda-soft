import UIKit

public enum AlbumsScene {
    
    // MARK: - Typealiases
    
    public typealias DeinitCompletion = ((_ vc: UIViewController) -> Void)?
    
    // MARK: -
    
    public enum Model {}
    public enum Event {}
}

// MARK: - Models

extension AlbumsScene.Model {
    
}

// MARK: - Events

extension AlbumsScene.Event {
    
    public typealias Model = AlbumsScene.Model
    
    // MARK: -
    
    public enum <#Event#> {
        public struct Request {}
        public struct Response {}
        public struct ViewModel {}
    }
}
